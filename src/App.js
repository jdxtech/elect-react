import React, { Component } from 'react';
import './App.css';
//import { render } from '@testing-library/react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Persons from './components/Persons';
import Navigation from './components/Navigation';


class App extends Component {

render() {
  return (      
    <BrowserRouter>
     <div>
       <Navigation />
         <Switch>
          <Route path="/persons" component={Persons}/>
        </Switch>
     </div> 
   </BrowserRouter>
  );
  }
}

export default App;

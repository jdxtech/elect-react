import React from 'react';
 
import { NavLink } from 'react-router-dom';
import '../App.css';
 
const Navigation = () => {
    return (
       <div>
<nav>
    <label className="logo">Electron</label>
  </nav>


<div id="mySidenav" className="sidenav">
  <NavLink to="/">Home</NavLink>
          <NavLink to="/persons">Persons</NavLink>
          <NavLink to="/contact">Areas</NavLink>
</div>
       </div>
    );
}
 
export default Navigation;
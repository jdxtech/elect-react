import React, { Component } from 'react';
import './Navigation';
//import { render } from '@testing-library/react';
import Search from "./sub_components/Search";


class Persons extends Component {

  constructor(props) {
    super(props);
    this.state = { apiResponse: "" };
}



callAPI() {
    fetch("http://localhost:9000/api/persons")
        .then(res => res.text())
        .then(res => this.setState({ apiResponse: res }));
}

componentWillMount() {
    this.callAPI();
}


renderTableHeader() {
    if(this.state.apiResponse){
    let header = JSON.parse(this.state.apiResponse);
    let data = Object.keys(header[0]);
    return data.map((key, index) => {
       return <th key={index}>{key.toUpperCase()}</th>
    })
   }
 }


 renderTableSearch() {
   const search = searchValue => {
      fetch(`http://localhost:9000/api/persons/?s=${searchValue}&apikey=4a3b711b`)
        .then(response => response.json())
        .then(jsonResponse => {
          if (jsonResponse.Response === "True") {
          } else {
          }
        });
       };

    if(this.state.apiResponse){
    let header = JSON.parse(this.state.apiResponse);
    let data = Object.keys(header[0]);
    return data.map((key, index) => {
       return <th key={index}>  <Search search={search} /></th>
    })
   }
 }




 renderTableData() {
    if(this.state.apiResponse){
        let personsList = JSON.parse(this.state.apiResponse);
    return personsList.map((student, index) => {
       const {    id, name, photo, related_name, related_type, age, mobile, email, caste, sub_caste, education, qualification, occupation, occupation_type, voterid, door_no, street, pincode, area_id, createdAt, updatedAt } = student //destructuring
       return (
          <tr key={id}>
             <td>{id}</td>
             <td>{name}</td>
             <td>{photo}</td>
             <td>{related_name}</td>
             <td>{related_type}</td>
             <td>{age}</td>
             <td>{mobile}</td>
             <td>{email}</td>
             <td>{caste}</td>
             <td>{sub_caste}</td>
             <td>{education}</td>
             <td>{qualification}</td>
             <td>{occupation}</td>
             <td>{occupation_type}</td>
             <td>{voterid}</td>
             <td>{door_no}</td>
             <td>{street}</td>
             <td>{pincode}</td>
             <td>{area_id}</td>
             <td>{createdAt}</td>
             <td>{updatedAt}</td>
          </tr>
       )
    });
   }
 }






render() {

    const divStyle={
        overflowX: 'auto'
      };
    return(
      <div className="main"  style={divStyle}>
        <table className="table" id='students'>
           <tbody>
              <tr>{this.renderTableHeader()}</tr>
              <tr>{this.renderTableSearch()}</tr>
              {this.renderTableData()}
           </tbody>
        </table>
        </div>
    );
  }
}

export default Persons;
